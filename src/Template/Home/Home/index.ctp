<section id="content" class="intro-section">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-2">
            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="content-left">
                        <div class="form-bottom twitter animated bounceIn">
                            <div class="logo-content">
                                <?= $this->Html->image('highlight-logo.png', ['alt' => 'WAHANA', 'class' => 'img-responsive animated fadeInDown highlight']); ?>
                                <?= $this->Html->image('logo.png', ['alt' => 'WAHANA', 'class' => 'img-responsive animated fadeInLeft']); ?>
                            </div>
                            <div class="wording animated fadeInUp">
                                <h3>BERHADIAH SATU UNIT MOTOR ALL NEW HONDA<br> SONIC 150R, 12 HELM DAN 3 SMARTPHONE DARI <br> WAHANA HONDA!</h3>
                            </div>
                            <div class="block-content animated fadeInUp">
                                <span>AYO MAIN SEKARANG! &nbsp;<a class="btn-click-here" href="">KLIK DISINI</a></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="content-right">
                        <?= $this->Html->image('img-landing-sonic.png', ['alt' => 'WAHANA', 'class' => 'img-responsive animated fadeInRight']); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>