<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Answered Question'), ['controller' => 'AnsweredQuestion', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Answered Question'), ['controller' => 'AnsweredQuestion', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users Point'), ['controller' => 'UsersPoint', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Users Point'), ['controller' => 'UsersPoint', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="users index large-9 medium-8 columns content">
    <h3><?= __('Users') ?></h3>
    <table cellpadding="0" cellspacing="0" class='table'>
        <thead>
        <tr>
            <th><?= $this->Paginator->sort('ID') ?></th>
            <th><?= $this->Paginator->sort('user_login') ?></th>
            <th><?= $this->Paginator->sort('user_email') ?></th>
            <th><?= $this->Paginator->sort('user_phone') ?></th>
            <th><?= $this->Paginator->sort('user_domisili') ?></th>
            <th><?= $this->Paginator->sort('user_status') ?></th>
            <th><?= $this->Paginator->sort('role') ?></th>
            <th class="actions"><?= __('Actions') ?></th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($members as $member): ?>
            <tr>
                <td><?= $this->Number->format($member->ID) ?></td>
                <td><?= h($member->user_login) ?></td>
                <td><?= h($member->user_email) ?></td>
                <td><?= h($member->user_phone) ?></td>
                <td><?= h($member->user_domisili) ?></td>
                <td><?= h($member->user_status) ?></td>
                <td><?= h($member->role) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $member->ID]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $member->ID]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $member->ID], ['confirm' => __('Are you sure you want to delete # {0}?', $member->ID)]) ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
        </ul>
        <p><?= $this->Paginator->counter() ?></p>
    </div>
</div>
