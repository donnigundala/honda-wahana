<!-- jQuery -->
<?= $this->Html->script('jquery.js') . PHP_EOL; ?>

<!-- Bootstrap Core JavaScript -->
<?= $this->Html->script('bootstrap.min.js') . PHP_EOL; ?>

<!-- Scrolling Nav JavaScript -->
<?= $this->Html->script('jquery.easing.min.js') . PHP_EOL; ?>
<?= $this->Html->script('scrolling-nav.js') . PHP_EOL; ?>