<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->Html->charset() . PHP_EOL; ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?= $this->Html->meta('viewport', 'width=device-width, initial-scale=1') . PHP_EOL; ?>
    <?= $this->Html->meta('description', 'site description here') . PHP_EOL; ?>
    <?= $this->Html->meta('author', 'auhor name here') . PHP_EOL; ?>

    <title>WAHANA HONDA QUIZ</title>
    <?= $this->Html->meta('ico-wahana-2.png', 'img/ico-wahana-2.png', ['type' => 'icon']) . PHP_EOL; ?>

    <!-- Bootstrap Core CSS -->
    <?= $this->Html->css('bootstrap.min.css') . PHP_EOL; ?>
    <?= $this->Html->css('font-awesome.min.css') . PHP_EOL; ?>
    <?= $this->Html->css('main.css') . PHP_EOL; ?>
    <?= $this->Html->css('animate.min.css') . PHP_EOL; ?>


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">

<?= $this->fetch('content') ?>

<footer>
    <div class="bottom" align="center">
        <p>COPYRIGHT 2016. WAHANA HONDA QUIZ</p>
    </div>
</footer>

<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">REGISTER</h3>
                <p>DAPATKAN KODE UNIK BERHADIAH MOTOR DENGAN MENGISI FORM PESERTA RIDING TEST DI BAWAH INI</p>
            </div>
            <div class="modal-body">

                <form>
                    <div class="title"><i class="fa fa-user"></i>&nbsp;NAMA LENGKAP</div>
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="Nama Lengkap">
                    </div>
                    <div class="title"><i class="fa fa-home"></i>&nbsp;NAMA SEKOLAH</div>
                    <div class="form-group address">
                        <input class="form-control" type="text" placeholder="Nama Sekolah">
                    </div>
                    <div class="title"><i class="fa fa-phone"></i>&nbsp;NOMER TELEPON</div>
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="Nomer Telepon">
                    </div>
                    <div class="title"><i class="fa fa-envelope"></i>&nbsp;EMAIL</div>
                    <div class="form-group">
                        <input class="form-control" type="text" placeholder="Alamat Email">
                    </div>
                    <br>
                    <div align="center">
                        <a href="">DAPATKAN KODE UNIKMU!</a>
                    </div>
                </form>

            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="success" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">REGISTRASI BERHASIL. ANDA SUDAH DAPAT MENGIKUTI WAHANA HONDA QUIZ!</h3>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="tnc" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
                <h3 class="modal-title" id="lineModalLabel">SYARAT DAN KETENTUAN</h3>
            </div>
            <div class="modal-body tnc">
                <b>HONDA WAHANA Video Contest:</b>
                <p>1. Program HONDA WAHANA Video Contest ini diselenggarakan oleh PT. Astra Honda Motor</p>
                <p>2. Peserta program ini adalah warga negara Indonesia dan bertempat tinggal di wilayah Republik Indonesia.</p>
                <p>3. Peserta/pemain, wajib melakukan registrasi dengan biodata asli dan lengkap di laman website yang sudah disediakan</p>
                <p>4. Peserta/pemain juga wajib like akun fanpage Welovehonda Indonesia pada link http://bit.ly/wlhifb</p>
                <p>5. Peserta/pemain juga wajib follow akun twitter Welovehonda Indonesia pada link http://bit.ly/twitWLHI</p>
                <p>6. Periode program ini berlangsung dari 1 Agustus hingga 30 September 2016</p>
                <p>7. Lucky voter dipilih dari video yang berhasil mendapatkan views tertinggi sepanjang periode perlombaan. Akan ada 50 voter beruntung yang bisa mendapatkan voucher pulsa sebesar 50 ribu rupiah.</p>
                <p>8.Video dengan hasil voting terbanyak selama periode aktivasi berjalan sampai dengan selesai berhak mendapakat hadiang uang tunai Rp.5.000.000</p>
                <p>9. Dengan mengikuti aktivasi ini, peserta/pemain memberikan izin kepada pihak penyelenggara untuk menggunakan data peserta untuk kepentingan Astra Honda Motor</p>
                <p>10. Pihak penyelanggara berhak menghapus materi yang diikutsertakan jika dianggap tidak memenuhi salah satu atau lebih dari syarat dan ketentuan (T&amp;C) ini.</p>
                <p>11. Hadiah tidak dapat dipindahtangankan maupun ditukarkan dengan hadiah dalam bentuk apapun.</p>
                <p>12. Jika karena alasan apapun pemberian hadiah tidak dapat berjalan sesuai rencana, pihak penyelenggara memiliki hak untuk memodifikasi ketentuan pemberian hadiah yang senilai dengan hadiah yang telah dijanjikan.</p>
                <p>13. Pihak penyelenggara berhak untuk mendiskualifikasi peserta yang menurut pendapat pihak penyelenggara adalah milik atau digunakan oleh satu pihak untuk memfasilitasi terjadinya duplikasi peserta terhadap kompetisi ini.</p>
                <p>14. Pihak penyelenggara berhak untuk mengubah Syarat dan Ketentuan ini dari waktu ke waktu, dengan atau tanpa pemberitahuan terlebih dahulu.</p>
                <p>15. Apabila pemenang tidak dapat dihubungi (untuk keperluan verifikasi) terhitung sejak 7 (tujuh) hari setelah pengumuman, maka pihak pemenang terpilih tersebut dinyatakan gugur dan dapat digantikan dengan pemenang pilihan juri berikutnya.</p>
                <p>16. Pihak penyelenggara, menurut pertimbangannya sendiri, berhak untuk mendiskualifikasikan peserta maupun pemenang yang tidak memenuhi dan/atau melanggar ketentuan ini dan/atau dianggap melakukan kecurangan dan pihak penyelenggara berhak untuk memberikan hadiah kepada peserta lainnya.</p>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="question" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" align="center">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
            </div>
            <div class="modal-body question">
                <h2 class="quest">Apa yang dimaksud dengan eSP pada teknologi motor matic Honda ?</h2>
                <div class="answer">
                    <div class="form-group">
                        <label class="radio-choice">
                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1"> Enhanced Smart Power
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="radio-choice">
                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option2"> Economic Solution Power
                        </label>
                    </div>
                    <div class="form-group">
                        <label class="radio-choice">
                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option3"> Electronic Smart Power
                        </label>
                    </div>
                </div>
                <div class="btn-answer">
                    <a href="#">JAWAB</a>
                </div>
            </div>
        </div>
    </div>
</div>

<?= $this->Element('script'); ?>
</body>

</html>