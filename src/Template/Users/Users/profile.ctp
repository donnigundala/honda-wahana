<section id="content" class="intro-section registration">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <div class="bar-top animated bounceIn">
                        <div class="logo-content">
                            <?= $this->Html->image('logo-highlight.png', ['class' => 'img-responsive animated fadeInDown', 'alt' => 'WAHANA']); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="bar-top animated bounceIn">
                        <div class="bar-top-content information icon">
                            <a data-toggle="modal" data-target="#tnc" href=""><i class="fa fa-2x fa-globe"></i></a>
                            <a data-toggle="modal" data-target="#tnc" href=""><i class="fa fa-2x fa-trophy"></i></a>
                            <a data-toggle="modal" data-target="#tnc" href=""><i class="fa fa-2x fa-user"></i></a>
                            <a data-toggle="modal" data-target="#tnc" href=""><i class="fa fa-2x fa-info-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" align="center">
            <div class="row">
                <div class="block-content-profile" align="center">
                    <div class="content-profile" align="center">
                        <div class="block-img-profile" align="center">
                            <?php if($user->user_image == null) : ?>
                                <?= $this->Html->image('icon-rofile.png', ['class' => 'img-responsive', 'alt' => 'WAHANA']); ?>
                            <?php else: ?>
                                <img src="<?= $user->user_image; ?>" class="img-responsive" alt="WAHANA"/>
                            <?php endif; ?>
                        </div>
                        <h3 class="name"><?= $user->display_name; ?></h3>
                        <h4 class="telephone"><?= $user->user_phone; ?></h4>
                        <h4 class="location"><?= $user->user_domisili; ?></h4>
                        <h4 class="point-caption">JUMLAH POIN</h4>
                        <h1 class="point">100</h1>
                        <div class="tab-profile" align="center">
                            <div class="panel with-nav-tabs panel-primary">
                                <div class="panel-heading">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a href="#tab1primary" data-toggle="tab">My History</a></li>
                                        <li><a href="#tab2primary" data-toggle="tab">Leader Board</a></li>
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="tab1primary">
                                            <table id="history" class="table table-bordred table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
                                                    <td align="right"><i class="fa fa-2x fa-check green"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</td>
                                                    <td align="right"><i class="fa fa-2x fa-check green"></i></td>
                                                </tr>
                                                <tr>
                                                    <td>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</td>
                                                    <td align="right"><i class="fa fa-2x fa-times red"></i></td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="load-more">
                                                <a href="#">Load More</a>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="tab2primary">
                                            <table id="leaderboard" class="table table-bordred table-striped">
                                                <tbody>
                                                <tr>
                                                    <td>1.</td>
                                                    <td>Lorem ipsum dolor sit amet, consectetur adipiscing elit</td>
                                                    <td align="right">150</td>
                                                </tr>
                                                <tr>
                                                    <td>2.</td>
                                                    <td>Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua</td>
                                                    <td align="right">100</td>
                                                </tr>
                                                <tr>
                                                    <td>3.</td>
                                                    <td>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris</td>
                                                    <td align="right">50</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                            <div class="load-more">
                                                <a href="#">Load More</a>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>