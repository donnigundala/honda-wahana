<section id="content" class="intro-section registration">
    <div class="container">
        <div class="col-md-12">
            <div class="col-md-6">
                <div class="row">
                    <div class="bar-top animated bounceIn">
                        <div class="logo-content">
                            <?= $this->Html->image('logo-highlight.png', ['class' => 'img-responsive animated fadeInDown', 'alt' => 'WAHANA']); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="row">
                    <div class="bar-top animated bounceIn">
                        <div class="bar-top-content information">
                            <a data-toggle="modal" data-target="#tnc" href=""><i class="fa fa-2x fa-info-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">
            <div class="col-md-12">
                <span class="text-center text-danger">
                <?= $this->Flash->render(); ?>
                    </span>
            </div>
        </div>

        <div class="col-md-12 registration-login">
            <div class="col-md-1">
            </div>
            <div class="col-md-5">
                <div class="row">
                    <div class="content-left">
                        <div class="form-bottom twitter animated bounceIn">
                            <div class="wording animated fadeInUp">
                                <h2>REGISTER</h2>
                                <p>Isi formulir pendaftaran dibawah ini.</p>
                            </div>
                            <div class="block-content animated fadeInUp">
                                <div class="form-bottom animated bounceIn">

                                    <?= $this->Form->create('', ['class'=>'registration-form', 'role' => 'form', 'enctype' => 'multipart/form-data']); ?>
                                        <div class="form-group upload">
                                            <p>Photo Profile</p>
                                            <?= $this->Form->file('user_image', ['id' => 'profile']); ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $this->Form->input('display_name', ['class' => 'form-control', 'id' => 'name', 'placeholder' => 'Nama Lengkap*', 'label' => false]); ?>
                                        </div>

                                        <div class="form-group">
                                            <?= $this->Form->input('user_address', ['class' => 'form-control', 'id' => 'user_address', 'placeholder' => 'Alamat Lengkap*', 'label' => false]); ?>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <?= $this->Form->input('user_phone', ['class' => 'form-control', 'id' => 'phone', 'placeholder' => 'Nomor Telepon*', 'label' => false]); ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <?= $this->Form->input('user_email', ['class' => 'form-control', 'id' => 'email', 'placeholder' => 'Alamat Email*', 'label' => false]); ?>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <?php
                                                        $option = ['jakarta' => 'Jakarta', 'tanggerang' => 'Tanggerang'];
                                                        echo $this->form->select('user_domisili', $option, ['class' => 'form-control nocorner']);
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <?= $this->Form->input('user_twitter', ['class' => 'form-control', 'id' => 'user_twitter', 'placeholder' => 'Akun Twitter*', 'label' => false]); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <?= $this->Form->input('user_login', ['class' => 'form-control', 'id' => 'user_login', 'placeholder' => 'Username*', 'label' => false]); ?>
                                                </div>
                                            </div>
                                            <div class="col-xs-12 col-sm-6 col-md-6">
                                                <div class="form-group">
                                                    <?= $this->Form->password('user_pass', ['class' => 'form-control', 'id' => 'user_pass', 'placeholder' => 'Password*', 'label' => false]); ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="block-content animated bounceIn" align="center">
                                            <div class="form-group">
                                                <div class="checkbox">
                                                    <label>
                                                        <?= $this->Form->checkbox('user_agreement', ['hiddenField' => false]); ?> <span class="text-muted"><b>Saya setuju dengan Syarat dan Ketentuan</b></span>
                                                    </label>
                                                </div>
                                            </div>
                                            <!--<a class="btn-registration" data-toggle="modal" data-target="#success" href="">KIRIM</a>-->
                                            <?= $this->Form->hidden('formRequest', ['value' => 'register']); ?>
                                            <?= $this->Form->button(__('KIRIM'), ['class'=>'btn-registration btn-block']); ?>
                                        </div>
                                    <?= $this->Form->end(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
            </div>

            <div class="col-md-4">
                <div class="row">
                    <div class="content-right">
                        <div class="wording animated fadeInUp">
                            <h2>LOGIN</h2>
                            <p>Sudah mendaftar? silahkan login untuk melanjukan quiz.</p>
                        </div>
                        <div class="block-content animated fadeInUp">
                            <div class="animated bounceIn">
                                <?= $this->Form->create(null, ['class'=>'registration-form', 'role' => 'form']); ?>
                                    <div class="form-group">
                                        <?= $this->Form->input('user_login', ['class' => 'form-control', 'id' => 'form-username', 'placeholder' => 'Username', 'label' => false]) . PHP_EOL; ?>
                                    </div>
                                    <div class="form-group">
                                        <?= $this->Form->password('user_pass', ['class' => 'form-control', 'id' => 'form-password', 'placeholder' => 'Password', 'label' => false]) . PHP_EOL; ?>
                                    </div>
                                    <!--<div class="form-group">
                                        <div class="img-captcha" align="center">
                                            <?/*= $this->Html->image('img-captcha.jpg', ['alt' => 'WAHANA', 'class' => 'img-responsive captcha']); */?>
                                        </div>
                                        <?/*= $this->Form->input('captcha', ['class' => 'form-control captcha', 'id' => 'captcha', 'placeholder' => 'Insert Captcha']); */?>
                                    </div>-->
                                    <div class="block-content animated bounceIn" align="center">
                                        <?= $this->Form->hidden('formRequest', ['value' => 'login']); ?>
                                        <?= $this->Form->button(__('Login'), ['class'=>'btn-registration btn-block']) . PHP_EOL; ?>
                                    </div>
                                <?= $this->Form->end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-1">
            </div>
        </div>
    </div>
</section>