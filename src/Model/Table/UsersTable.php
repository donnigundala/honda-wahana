<?php
namespace App\Model\Table;

use App\Model\Entity\User;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WpUsers Model
 *
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('wp_users');
        $this->displayField('ID');
        $this->primaryKey('ID');

        $this->addAssociations([
            'belongsTo' => [
                'AnsweredQuestions' => [
                    'className' => 'AnsweredQuestions',
                    'foreignKey' => 'ID',
                    'bindingKey' => 'user_id',
                ],
                'UsersPoint' => [
                    'className' => 'UsersPoint',
                    'foreignKey' => 'ID',
                    'bindingKey' => 'user_id'
                ]
            ]
        ]);

        $this->addBehavior('Utils.Uploadable', [
            'user_image' => [
                'fields' => [
                    'fileName' => 'user_image'
                ],
                'removeFileOnUpdate' => true,
                'removeFileOnDelete' => true,
                'field' => 'user_login',
                'path' => '{ROOT}{DS}{WEBROOT}{DS}img{DS}user_image{DS}{field}{DS}',
                'fileName' => '{field}.{extension}',
            ]
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->allowEmpty('ID', 'create');

        $validator // required user full name
        ->requirePresence('display_name', 'create')
            ->notEmpty('display_name')
            ->add('display_name', [
                'minLength' => [
                    'rule' => ['minLength', 2],
                    'last' => 'true',
                    'message' => 'Nama minimal harus 2 character'
                ],
                'maxLength' => [
                    'rule' => ['maxLength', 60],
                    'message' => 'Nama Terlalu Panjang'
                ]
            ]);

        $validator // required username
        ->requirePresence('user_login', 'create')
            ->notEmpty('user_login')
            ->add('user_login', [
                'minLength' => [
                    'rule' => ['minLength', 4],
                    'message' => 'Nama minimal harus 4 character'
                ],
            ]);

        $validator // required user password
        ->requirePresence('user_pass', 'create')
            ->notEmpty('user_pass')
            ->add('user_pass', [
                'minLength' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Nama minimal harus 6 character'
                ]
            ]);

        $validator // required user email
        ->requirePresence('user_email', 'create')
            ->notEmpty('user_email')
            ->add('user_email', 'validFormat', [
                'rule' => 'email',
                'message' => 'Email tidak valid'
            ]);

        $validator // required user phone
        ->requirePresence('user_phone', 'create')
            ->notEmpty('user_phone')
            ->add('user_phone', [
                'numeric' => [
                    'rule' => 'numeric',
                    'last' => true,
                    'message' => 'Isi hanya dengan angka'
                ],
                'minLength' => [
                    'rule' => ['minLength', 6],
                    'message' => 'No. Telp minimal harus 6 angka'
                ]
            ]);
        
        $validator
            ->requirePresence('user_address')
            ->notEmpty('user_address');

        $validator // required user domisili
            ->requirePresence('user_domisili', 'create')
            ->notEmpty('user_domisili');

        $validator
            ->allowEmpty('user_image', 'create');

        $validator
            ->requirePresence('user_agreement');

        $validator
            ->requirePresence('user_agreement')
            ->notEmpty('user_agreement');

        //$validator->allowEmpty('user_nicename', 'create');

        //$validator->allowEmpty('user_url', 'create');

        //$validator->dateTime('user_registered')->requirePresence('user_registered', 'create')->notEmpty('user_registered');

        //$validator->requirePresence('user_activation_key', 'create')->notEmpty('user_activation_key');

        //$validator->integer('user_status')->requirePresence('user_status', 'create')->notEmpty('user_status');

        return $validator;
    }
}
