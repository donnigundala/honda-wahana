<?php
namespace App\Model\Table;

use App\Model\Entity\Admin;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WpUsers Model
 *
 */
class AdminsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->table('admins');
        $this->displayField('id');
        $this->primaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator->allowEmpty('id', 'create');

        $validator // required username
        ->requirePresence('username', 'create')
            ->notEmpty('username')
            ->add('username', [
                'minLength' => [
                    'rule' => ['minLength', 4],
                    'message' => 'Username minimal harus 4 character'
                ],
            ]);

        $validator // required user password
        ->requirePresence('password', 'create')
            ->notEmpty('password')
            ->add('password', [
                'minLength' => [
                    'rule' => ['minLength', 6],
                    'message' => 'Password Minimal 6 karakter'
                ]
            ]);

        $validator
            ->notEmpty('role', 'A role is required')
            ->add('role', 'inList', [
                'rule' => ['inList', ['admin', 'author']],
                'message' => 'Please enter a valid role'
            ]);

        return $validator;
    }
}
