<?php
namespace App\Controller\Admin;

use Cake\Core\Configure;
use App\Controller\AdminController;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;

class DashboardController extends AdminController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    public function index()
    {

    }
}