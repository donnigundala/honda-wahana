<?php
namespace App\Controller\Admin;

use Cake\Core\Configure;
use App\Controller\AdminController;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\Event\Event;

class QuestionsController extends AdminController
{
    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
    }

    /* Display all available Question List */
    public function display()
    {
        $questions = $this->paginate($this->Questions);

        $this->set(compact('questions'));
        $this->set('_serialize', ['questions']);
    }

    /* Add new Question List*/
    public function add()
    {

    }

    /* Edit Question */
    public function edit($id)
    {

    }

    /* Delete Question */
    public function delete($id)
    {

    }
}